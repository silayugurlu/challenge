package com.mobiquityinc.packer;

import com.mobiquityinc.exception.APIException;
import com.mobiquityinc.exception.InputFileException;
import com.mobiquityinc.exception.internal.InternalException;
import com.mobiquityinc.packer.factory.ServiceFactory;
import com.mobiquityinc.packer.manager.PackerManager;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Packer {

    /**
     *
     * Given a set of items in the file format below, each with a weight and a cost, determines the index of items to include in a package so that the total weight is less than or equal to a given limit and the total cost is maximized.
     *
     * input file format
     * 81 : (1,53.38,€45) (2,88.62,€98) (3,78.48,€3) (4,72.30,€76) (5,30.18,€9) (6,46.34,€48)
     * 8 : (1,15.3,€34)
     *
     * output
     * 4,7
     * -
     *
     * @param filePath
     * @return index of items
     * @throws APIException
     */

    public static String pack(String filePath) throws APIException {

        try {
            Stream<String> lines = Files.lines(Paths.get(filePath));

            PackerManager packerManager = (PackerManager) ServiceFactory.builder().
                            typeParameterClass(PackerManager.class).build().createService();



            String result = lines
                    .map(packerManager :: preparePackage)
                    .collect(Collectors.joining("\n"));



            lines.close();
            return result;
        } catch(IOException e) {
            throw new InputFileException("Error reading file from path: "+filePath);
        }catch(InternalException e) {
            throw new APIException(e.getMessage());
        }catch(Exception e) {
            throw new APIException(e.getMessage());
        }
    }

}
