package com.mobiquityinc.packer.manager;

import com.mobiquityinc.packer.factory.ServiceFactory;
import com.mobiquityinc.packer.model.Item;
import com.mobiquityinc.packer.model.PackageData;
import com.mobiquityinc.packer.output.ItemWriter;
import com.mobiquityinc.packer.parser.PackageParser;
import com.mobiquityinc.packer.solution.PackageOrganizer;


import java.util.List;

public class PackerManagerImpl implements PackerManager {



    /**
     * this static instance will hold the single object
     */
    private static PackerManager instance;


    /**
     *
     * @return Application object's thread safe static singleton instance
     */
    public static PackerManager getInstance() {
        // check if the instance is null
        if (instance == null) {
            // synchronized block is inside the if condition
            // because the synchronized block might slow down this method's call
            synchronized (PackerManager.class) {
                // the catch with this approach is we had to check the same condition here again
                if (instance == null) {
                    // create and assign the instance
                    instance = new PackerManagerImpl();
                }
            }
        }
        // return instance
        return instance;
    }



    public String preparePackage(String line) {

        PackageParser packageParser= (PackageParser) ServiceFactory.builder().
                typeParameterClass(PackageParser.class).build().createService();

        PackageOrganizer packageOrganizer= (PackageOrganizer)ServiceFactory.builder().
                typeParameterClass(PackageOrganizer.class).build().createService();

        ItemWriter itemWriter= (ItemWriter)ServiceFactory.builder().
                typeParameterClass(ItemWriter.class).build().createService();


        //parse input data
        PackageData packageData =packageParser.parse(line);

        //find suitable items to put package
        List<Item> itemsInPackage = packageOrganizer.organize(packageData);

        //convert result items to string
        String result =itemWriter.writeItem(itemsInPackage);

        return result;
    }

}
