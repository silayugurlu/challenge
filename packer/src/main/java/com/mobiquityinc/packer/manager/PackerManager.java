package com.mobiquityinc.packer.manager;

import com.mobiquityinc.exception.APIException;

public interface PackerManager {


     /**
      * parse data, find items, return items in package
      * @param line
      * @return
      */
     String preparePackage(String line);

}
