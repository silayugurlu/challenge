package com.mobiquityinc.packer.solution;

import com.mobiquityinc.packer.model.Item;
import com.mobiquityinc.packer.model.PackageData;


import java.util.ArrayList;
import java.util.Collections;
import java.util.List;



public class PackageOrganizerImpl implements  PackageOrganizer{



    /**
     * this static instance will hold the single object
     */
    private static PackageOrganizer instance;


    /**
     *
     * @return Application object's thread safe static singleton instance
     */
    public static PackageOrganizer getInstance() {
        // check if the instance is null
        if (instance == null) {
            // synchronized block is inside the if condition
            // because the synchronized block might slow down this method's call
            synchronized (PackageOrganizer.class) {
                // the catch with this approach is we had to check the same condition here again
                if (instance == null) {
                    // create and assign the instance
                    instance = new PackageOrganizerImpl();
                }
            }
        }
        // return instance
        return instance;
    }

    @Override
    public List<Item> organize(PackageData packageData) {


        int maxWeight = packageData.getMaxWeight();

        List<Item> items = packageData.getItems();
        if(items == null) return null;

        int itemCount = packageData.getItems().size();


        // matrix to store the max value at each n-th item
        int[][] matrix = new int[itemCount + 1][maxWeight + 1];

        // first line is initialized to 0
        for (int i = 0; i <= maxWeight; i++)
            matrix[0][i] = 0;

        // iterate on items
        for (int i = 1; i <= itemCount; i++) {
            // iterate on each weight
            for (int j = 0; j <= maxWeight; j++) {
                if ( items.get(i-1).getWeight() > j)
                    matrix[i][j] = matrix[i-1][j];
                else
                    // maximize value at this rank in the matrix
                    matrix[i][j] = Math.max(matrix[i-1][j], matrix[i-1][j - new Double(items.get(i-1).getWeight()).intValue()]
                            + items.get(i-1).getCost());
            }
        }

        List<Item> itemsSolution = new ArrayList<>();

        // find selected items
        int tempW = maxWeight;

        for (int x = itemCount; x > 0; x--){
            Item item = items.get(x-1);
            int weightOfItem =new Double(item.getWeight()).intValue();
            int costOfItem=item.getCost();

            if ((tempW-weightOfItem >= 0) && (matrix[x][tempW] - matrix[x-1][tempW-weightOfItem] == costOfItem) ){
                itemsSolution.add(item); //store current index and increment y
                tempW-=weightOfItem;
            }
        }

        Collections.reverse(itemsSolution);
        return itemsSolution;
    }


}
