package com.mobiquityinc.packer.solution;

import com.mobiquityinc.packer.model.Item;
import com.mobiquityinc.packer.model.PackageData;

import java.util.List;

public interface PackageOrganizer {

    List<Item> organize(PackageData packageData);
}
