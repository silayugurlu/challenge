package com.mobiquityinc.packer.output;

import com.mobiquityinc.packer.model.Item;

import java.util.List;

public interface ItemWriter {

     /**
      * convert comma seperated index of items
      *
      * @param items
      * @return
      */
     String writeItem(List<Item> items);
}
