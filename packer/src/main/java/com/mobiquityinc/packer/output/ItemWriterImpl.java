package com.mobiquityinc.packer.output;

import com.mobiquityinc.packer.model.Item;
import java.util.List;
import java.util.stream.Collectors;



public class ItemWriterImpl implements ItemWriter {



    /**
     * this static instance will holt the single object
     */
    private static ItemWriter instance;


    /**
     *
     * @return Application object's thread safe static singleton instance
     */
    public static ItemWriter getInstance() {
        // check if the instance is null
        if (instance == null) {
            // synchronized block is inside the if condition
            // thats because the synchronized block might slow down this method's call
            synchronized (ItemWriter.class) {
                // the catch with this approach is we had to check the same condition here again
                if (instance == null) {
                    // create and assign the instance
                    instance = new ItemWriterImpl();
                }
            }
        }
        // return instance
        return instance;
    }



    @Override
    public String writeItem(List<Item> items) {

        if(items == null || items.size() == 0){
            return "-";
        }
        String itemsCommaSeperated = items.stream()
                .map(s -> s.getIndex())
                .collect(Collectors.joining(","));

        return itemsCommaSeperated;
    }
}
