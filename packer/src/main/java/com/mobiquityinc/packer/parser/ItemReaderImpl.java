package com.mobiquityinc.packer.parser;


import com.mobiquityinc.exception.internal.InvalidItemDataException;
import com.mobiquityinc.exception.internal.MaxItemCostExceededException;
import com.mobiquityinc.exception.internal.MaxItemWeightExceededException;
import com.mobiquityinc.packer.Constants;
import com.mobiquityinc.packer.model.Item;

public class ItemReaderImpl implements  ItemReader {

    /**
     * this static instance will hold the single object
     */
    private static ItemReader instance;


    /**
     *
     * @return Application object's thread safe static singleton instance
     */
    public static ItemReader getInstance() {
        // check if the instance is null
        if (instance == null) {
            // synchronized block is inside the if condition
            // thats because the synchronized block might slow down this method's call
            synchronized (ItemReader.class) {
                // the catch with this approach is we had to check the same condition here again
                if (instance == null) {
                    // create and assign the instance
                    instance = new ItemReaderImpl();
                }
            }
        }
        // return instance
        return instance;
    }


    /**
     *
     * @param itemStr
     * @return
     */
    public Item parseItem(String itemStr){

        // get item values in parentheses

        String item;
        try {
             item = itemStr.substring(itemStr.indexOf('(') + 1, itemStr.indexOf(')'));
        }catch(Exception e){
            throw new InvalidItemDataException("Invalid data for items");
        }

        String[] values = item.split(",",3);

        // get index of item
        String index = values[0];

        double weight;
        int cost;
        try {
            //get weight of item
            weight = Double.parseDouble(values[1]);

            // get cost without first character currency
            cost = Integer.parseInt(values[2].substring(Constants.CURRENCY_CHAR_COUNT));

        }catch(Exception e){
            throw new InvalidItemDataException("Wrong number formats for item "+item);
        }

        if(weight > Constants.MAX_ITEM_WEIGHT) throw new MaxItemWeightExceededException("Max item weight exceeded, max value is "+Constants.MAX_ITEM_WEIGHT);

        if(cost > Constants.MAX_ITEM_COST) throw new MaxItemCostExceededException("Max item cost exceeded, max value is " +Constants.MAX_ITEM_COST);

        return  Item.builder().index(index).weight(weight).cost(cost).build();
    }
}
