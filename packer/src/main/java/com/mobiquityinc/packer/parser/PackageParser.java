package com.mobiquityinc.packer.parser;

import com.mobiquityinc.exception.internal.*;
import com.mobiquityinc.packer.model.PackageData;


public interface PackageParser {

    /**
     * parse package information from string
     *
     *
     * @param line
     * @return
     */
    PackageData parse(String line);

}
