package com.mobiquityinc.packer.parser;

import com.mobiquityinc.exception.internal.InvalidInputValueException;
import com.mobiquityinc.exception.internal.MaxItemCountExceededException;
import com.mobiquityinc.exception.internal.MaxPackageWeightExceededException;
import com.mobiquityinc.packer.Constants;
import com.mobiquityinc.packer.factory.ServiceFactory;
import com.mobiquityinc.packer.model.Item;
import com.mobiquityinc.packer.model.PackageData;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;



public class PackageParserImpl implements  PackageParser{


    /**
     * this static instance will hold the single object
     */
    private static PackageParser instance;


    /**
     *
     * @return Application object's thread safe static singleton instance
     */
    public static PackageParser getInstance() {
        // check if the instance is null
        if (instance == null) {
            // synchronized block is inside the if condition
            // thats because the synchronized block might slow down this method's call
            synchronized (PackageParser.class) {
                // the catch with this approach is we had to check the same condition here again
                if (instance == null) {
                    // create and assign the instance
                    instance = new PackageParserImpl();
                }
            }
        }
        // return instance, at this point it should not be null
        return instance;
    }


    /**
     * parse package information from string
     *
     *  81 : (1,53.38,€45) (2,88.62,€98) (3,78.48,€3) (4,72.30,€76) (5,30.18,€9) (6,46.34,€48)
     *
     * @param line
     * @return
     */
    public PackageData parse(String line) {

        // get maximum weight of the package and check for validation
        String [] splittedLine = line.split(" :",2);
        Integer maxWeight;
        try {
            maxWeight = Integer.parseInt(splittedLine[0]);
        }catch(Exception e){
            throw new InvalidInputValueException("Invalid value in input file: "+splittedLine[0]);
        }
        if(maxWeight > Constants.MAX_PACKAGE_WEIGHT) throw new MaxPackageWeightExceededException("Max package weight exceeded, max value is "+Constants.MAX_PACKAGE_WEIGHT);

        // get items can be put in package
        String itemsLine = splittedLine[1];

        List<Item> items = new ArrayList<>();

        ItemReader itemReader = (ItemReader) ServiceFactory.builder().
                typeParameterClass(ItemReader.class).build().createService();

        String [] itemsStr =itemsLine.trim().split(" ");


        if(itemsStr.length > Constants.MAX_ITEM_COUNT)  throw new MaxItemCountExceededException("Max item count exceeded, max value is "+Constants.MAX_ITEM_COUNT);


        Arrays.stream(itemsStr).forEach(s -> items.add(itemReader.parseItem(s)));


        // create package data involves max weight and items
        PackageData packageData = new PackageData(maxWeight,items);
        return packageData;
    }

}
