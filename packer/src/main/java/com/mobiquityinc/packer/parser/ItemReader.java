package com.mobiquityinc.packer.parser;


import com.mobiquityinc.exception.internal.MaxItemCostExceededException;
import com.mobiquityinc.exception.internal.MaxItemWeightExceededException;
import com.mobiquityinc.packer.model.Item;



public interface ItemReader {


    /**
     * parse itemstr string and return new item.
     *
     * @param itemStr
     * @return
     */
    Item parseItem(String itemStr);
}
