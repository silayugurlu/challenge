package com.mobiquityinc.packer.model;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@Builder
public class Item {

    private String index;
    private double weight;
    private int cost;



}
