package com.mobiquityinc.packer.model;

import com.mobiquityinc.packer.model.Item;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
public class PackageData {

    private int maxWeight;
    private List<Item> items;
}
