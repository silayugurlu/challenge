package com.mobiquityinc.packer;

public class Constants {

    public static int MAX_ITEM_COST = 100;

    public static int MAX_ITEM_WEIGHT = 100;

    public static int MAX_PACKAGE_WEIGHT = 100;

    public static int MAX_ITEM_COUNT = 15;

    public static int CURRENCY_CHAR_COUNT = 1; //€45
}
