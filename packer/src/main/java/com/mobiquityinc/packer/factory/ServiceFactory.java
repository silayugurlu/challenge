package com.mobiquityinc.packer.factory;


import com.mobiquityinc.packer.manager.PackerManager;
import com.mobiquityinc.packer.manager.PackerManagerImpl;
import com.mobiquityinc.packer.output.ItemWriter;
import com.mobiquityinc.packer.output.ItemWriterImpl;
import com.mobiquityinc.packer.parser.ItemReader;
import com.mobiquityinc.packer.parser.ItemReaderImpl;
import com.mobiquityinc.packer.parser.PackageParser;
import com.mobiquityinc.packer.parser.PackageParserImpl;
import com.mobiquityinc.packer.solution.PackageOrganizer;
import com.mobiquityinc.packer.solution.PackageOrganizerImpl;
import lombok.Builder;
import lombok.Setter;

@Setter
@Builder
public class ServiceFactory{

     Class typeParameterClass;

    public  Object createService(){

        if(typeParameterClass.equals(ItemReader.class)){
            return ItemReaderImpl.getInstance();
        }
        if(typeParameterClass.equals(ItemWriter.class)){
            return  ItemWriterImpl.getInstance();
        }
        if(typeParameterClass.equals(PackerManager.class)){
            return  PackerManagerImpl.getInstance();
        }
        if(typeParameterClass.equals(PackageOrganizer.class)){
            return  PackageOrganizerImpl.getInstance();
        }
        if(typeParameterClass.equals(PackageParser.class)){
            return  PackageParserImpl.getInstance();
        }
        return null;
    }
}
