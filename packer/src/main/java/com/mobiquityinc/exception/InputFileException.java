package com.mobiquityinc.exception;

public class InputFileException extends APIException {


    public InputFileException(String message) {
        super(message);
    }
}
