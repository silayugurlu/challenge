package com.mobiquityinc.exception.internal;

public class MaxPackageWeightExceededException extends InternalException {
    public MaxPackageWeightExceededException(String message) {
        super(message);
    }
}
