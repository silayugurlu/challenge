package com.mobiquityinc.exception.internal;

public class MaxItemCountExceededException extends InternalException {
    public MaxItemCountExceededException(String message) {
        super(message);
    }
}
