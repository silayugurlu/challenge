package com.mobiquityinc.exception.internal;

public class MaxItemWeightExceededException extends InternalException {
    public MaxItemWeightExceededException(String message) {
        super(message);
    }
}
