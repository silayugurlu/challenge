package com.mobiquityinc.exception.internal;

public class InvalidItemDataException extends InternalException {
    public InvalidItemDataException(String message) {
        super(message);
    }
}