package com.mobiquityinc.exception.internal;

public class InvalidInputValueException extends InternalException {
    public InvalidInputValueException(String message) {
        super(message);
    }
}
