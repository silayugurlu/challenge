package com.mobiquityinc.exception.internal;

public class InternalException extends RuntimeException {

    public InternalException(String message){
        super(message);
    }
}
