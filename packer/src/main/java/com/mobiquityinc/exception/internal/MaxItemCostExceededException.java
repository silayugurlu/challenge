package com.mobiquityinc.exception.internal;

public class MaxItemCostExceededException extends InternalException {

    public MaxItemCostExceededException(String message){
        super(message);
    }
}
