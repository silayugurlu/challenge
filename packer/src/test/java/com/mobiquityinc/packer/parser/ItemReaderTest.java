package com.mobiquityinc.packer.parser;

import com.mobiquityinc.exception.internal.InvalidItemDataException;
import com.mobiquityinc.exception.internal.MaxItemCostExceededException;
import com.mobiquityinc.exception.internal.MaxItemWeightExceededException;
import com.mobiquityinc.packer.factory.ServiceFactory;
import com.mobiquityinc.packer.model.Item;
import org.junit.Test;

import static org.junit.Assert.assertEquals;


public class ItemReaderTest {

    private ItemReader itemReader = (ItemReader) ServiceFactory.builder().
            typeParameterClass(ItemReader.class).build().createService();

    @Test
    public void parseItemSuccessfully(){
        String itemStr="(7,81.80,€45)";
        Item result = itemReader.parseItem(itemStr);

        assertEquals("7",result.getIndex());
        assertEquals(81.80,result.getWeight(),0);
        assertEquals(45,result.getCost());
    }

    @Test(expected = MaxItemWeightExceededException.class)
    public void maxItemWeightExceeded(){
        String itemStr="(7,100.80,€45)";
        itemReader.parseItem(itemStr);
    }

    @Test(expected = MaxItemCostExceededException.class)
    public void maxItemCostExceeded(){
        String itemStr="(7,81.80,€101)";
        itemReader.parseItem(itemStr);
    }


    @Test(expected = InvalidItemDataException.class)
    public void invalidItemData(){
        String itemStr=" ";
        itemReader.parseItem(itemStr);
    }
    @Test(expected = InvalidItemDataException.class)
    public void invalidItemDataFormat(){
        String itemStr="(7,dadas,€101)";
        itemReader.parseItem(itemStr);
    }
}
