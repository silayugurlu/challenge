package com.mobiquityinc.packer.parser;

import com.mobiquityinc.exception.internal.InvalidInputValueException;
import com.mobiquityinc.exception.internal.MaxItemCountExceededException;
import com.mobiquityinc.exception.internal.MaxPackageWeightExceededException;
import com.mobiquityinc.packer.factory.ServiceFactory;
import com.mobiquityinc.packer.model.PackageData;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class PackageParserTest {

    private PackageParser packageParser = (PackageParser) ServiceFactory.builder().
            typeParameterClass(PackageParser.class).build().createService();


    @Test
    public void parsePackageSuccessfully(){
        String packageStr ="81 : (1,53.38,€45) (2,88.62,€98) (3,78.48,€3) (4,72.30,€76) (5,30.18,€9) (6,46.34,€48)";
        PackageData result = packageParser.parse(packageStr);

        assertEquals(81,result.getMaxWeight());
        assertEquals(6 ,result.getItems().size());


        assertEquals("1",result.getItems().get(0).getIndex());
        assertEquals(53.38,result.getItems().get(0).getWeight(),0);
        assertEquals(45,result.getItems().get(0).getCost());
    }

    @Test(expected = MaxPackageWeightExceededException.class)
    public void maxPackageWeightExceeded(){
        String packageStr ="101 : (1,53.38,€45) (2,88.62,€98) (3,78.48,€3) (4,72.30,€76) (5,30.18,€9) (6,46.34,€48)";
        packageParser.parse(packageStr);
    }

    @Test(expected = MaxItemCountExceededException.class)
    public void maxItemCountExceeded(){
        String packageStr ="81 : (1,53.38,€45) (2,88.62,€98) (3,78.48,€3) (4,72.30,€76) (5,30.18,€9) (6,46.34,€48) (1,53.38,€45) (2,88.62,€98) (3,78.48,€3) (4,72.30,€76) (5,30.18,€9) (6,46.34,€48)  (3,78.48,€3) (4,72.30,€76) (5,30.18,€9) (6,46.34,€48)";
        packageParser.parse(packageStr);
    }

    @Test(expected = InvalidInputValueException.class)
    public void invalidInputValue(){
        String packageStr =" : ";
        packageParser.parse(packageStr);
    }

    @Test(expected = InvalidInputValueException.class)
    public void invalidEmptyInputValue(){
        String packageStr ="  ";
        packageParser.parse(packageStr);
    }

}
