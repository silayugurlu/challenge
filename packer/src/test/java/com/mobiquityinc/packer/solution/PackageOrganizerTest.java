package com.mobiquityinc.packer.solution;

import com.mobiquityinc.packer.factory.ServiceFactory;
import com.mobiquityinc.packer.model.Item;
import com.mobiquityinc.packer.model.PackageData;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;


public class PackageOrganizerTest {

    private PackageOrganizer packageOrganizer = (PackageOrganizer) ServiceFactory.builder().
            typeParameterClass(PackageOrganizer.class).build().createService();

    @Test
    public void packItemsSuccessfully(){
        List items = new ArrayList<>();
        items.add(new Item("1",53.38,45));
        items.add(new Item("2",88.62,98));
        items.add(new Item("3",78.48,3));
        items.add(new Item("4",72.30,76));
        items.add(new Item("5",30.18,9));
        items.add(new Item("6",46.34,48));
        PackageData packageData = new PackageData(81,items);

        List<Item> result = packageOrganizer.organize(packageData);

        assertEquals(1,result.size());
        assertEquals("4",result.get(0).getIndex());
    }
    @Test
    public void packItemsSameCost(){

        List items = new ArrayList<>();
        items.add(new Item("1",90.72,13));
        items.add(new Item("2",33.80,40));
        items.add(new Item("3",43.15,10));
        items.add(new Item("4",37.97,16));
        items.add(new Item("5",46.81,36));
        items.add(new Item("6",48.77,79));
        items.add(new Item("7",81.80,45));
        items.add(new Item("8",19.36,79));
        items.add(new Item("9",6.76,64));
        PackageData packageData = new PackageData(56,items);

        List<Item> result = packageOrganizer.organize(packageData);

        assertEquals(2,result.size());
        assertEquals("8",result.get(0).getIndex());
        assertEquals("9",result.get(1).getIndex());

    }

    @Test
    public void packItemsNoResult(){
        List items = new ArrayList<>();
        items.add(new Item("1",53.38,45));
        PackageData packageData = new PackageData(8,items);
        List<Item> result = packageOrganizer.organize(packageData);
        assertEquals(0,result.size());
    }

}
