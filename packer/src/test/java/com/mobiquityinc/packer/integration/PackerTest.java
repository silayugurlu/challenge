package com.mobiquityinc.packer.integration;

import com.mobiquityinc.exception.APIException;
import com.mobiquityinc.packer.Packer;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class PackerTest {

    @Test
    public void packSuccessfully() throws APIException{

        String result = Packer.pack("src/test/resources/test_pack.txt");
        assertEquals("4\n" +
                    "-\n" +
                    "2,7\n" +
                    "8,9",result);

    }

    @Test(expected = APIException.class)
    public void packFileError() throws APIException{
        Packer.pack("src/test/resources/test_no_file.txt");
    }

    @Test(expected = APIException.class)
    public void packDataError() throws APIException{
        Packer.pack("src/test/resources/test_pack_error.txt");
    }
}
