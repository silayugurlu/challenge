package com.mobiquityinc.packer.output;

import com.mobiquityinc.packer.factory.ServiceFactory;
import com.mobiquityinc.packer.model.Item;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class ItemWriterTest {


    private ItemWriter itemWriter = (ItemWriter) ServiceFactory.builder().
            typeParameterClass(ItemWriter.class).build().createService();

    @Test
    public void writeItemsSuccessfully(){
        List items = new ArrayList<>();
        items.add(new Item("1",53.38,45));
        items.add(new Item("2",88.62,98));
        items.add(new Item("3",78.48,3));
        items.add(new Item("4",72.30,76));
        items.add(new Item("5",30.18,9));
        items.add(new Item("6",46.34,48));

        String result = itemWriter.writeItem(items);

        assertEquals("1,2,3,4,5,6",result);

    }
    @Test
    public void writeItemsNoResult(){
        List items = new ArrayList<>();
        String result = itemWriter.writeItem(items);

        assertEquals("-",result);
    }

    @Test
    public void writeItemsNull(){
        String result = itemWriter.writeItem(null);

        assertEquals("-",result);
    }
}
