package com.mobiquityinc.packer.manager;

import com.mobiquityinc.packer.factory.ServiceFactory;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class PackerManagerTest {

    PackerManager packerManager = (PackerManager) ServiceFactory.builder().
            typeParameterClass(PackerManager.class).build().createService();


    @Test
    public void managePackageSuccessfully(){
        String packageLine = "75 : (1,85.31,€29) (2,14.55,€74) (3,3.98,€16) (4,26.24,€55) (5,63.69,€52) (6,76.25,€75) (7,60.02,€74) (8,93.18,€35) (9,89.95,€78)";
        String result = packerManager.preparePackage(packageLine);
        assertEquals("2,7",result);
    }
}
