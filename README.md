Given a set of items, each with a weight and a cost, library determines the index items to include in a package so that the total weight is less than or equal to a given limit and the total cost is maximized.
Throws APIException


input file format 
      81 : (1,53.38,€45) (2,88.62,€98) (3,78.48,€3) (4,72.30,€76) (5,30.18,€9) (6,46.34,€48) 
      8 : (1,15.3,€34) 
output
      4,7
      - 
	  
The library can be used by adding maven dependency
	<dependency>
            <groupId>com.mobiquityinc</groupId>
            <artifactId>packer</artifactId>
            <version>1.0</version>
  	</dependency>
    
    
You can use com.mobiquityinc.packer.Packer class pack method gets filepath parameter
	  
